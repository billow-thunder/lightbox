# lightbox

### Installation

Add the service provider to config/app.php

```php
Billow\LightBox\LightBoxProvider::class
```

```js
mix.copy('vendor/billowapp/lightbox/src/js', 'resources/assets/js/vendor/billow/lightbox')
```

```php
art migrate
```

### Usage

feed the lightbox a gallery, which will contain Path information as well as an Array of images

```php
<lightbox :gallery="{{ $gallery->toJson() }}"></lightbox>
<gallery-index :galleries="{{ $galleries->toJson() }}"></gallery-index>
<edit-gallery :gallery="{{ $gallery->toJson() }}"></edit-gallery>
```

#### Vue Components

```js

 Admin

 Vue.component('gallery-index', require('./vendor/billow/lightbox/GalleryIndex.vue'))
 Vue.component('edit-gallery', require('./vendor/billow/lightbox/EditGallery.vue')) 

 Public

 Vue.component('lightbox', require('./vendor/billow/lightbox/LightBox.vue'))
```

### Controller Example

Sending the gallery to the blade.

```php
public function getGallery()
{
	$gallery = Gallery::whereId(1)->with('images')->first();
	return view('gh::pages.gallery', compact('gallery'));
}
```

### Model Binding Names

Gallery::class = {b_gallery}
GalleryImage::class = {b_gallery_image}

