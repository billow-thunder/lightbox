@extends(env('ADMIN_LAYOUT'))

@section('content')
	<div class="columns">
		<div class="column">
			<h1>Create Gallery</h1>
		</div>
		<div class="column">
			<a class="button small mt" href="/admin/galleries/">Galleries</a>
		</div>
	</div>
	<create-gallery></create-gallery>
@stop
