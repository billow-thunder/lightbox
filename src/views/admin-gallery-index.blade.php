@extends(env('ADMIN_LAYOUT'))

@section('content')
	<div class="columns">
		<div class="column">
			<h1>Galleries</h1>
		</div>
		<div class="column">
			<a class="button small mt" href="/admin/gallery/categories">Categories</a>
			<a class="button small mt" href="/admin/galleries/create">Create Gallery</a>
		</div>
	</div>
	<admin-gallery-index></admin-gallery-index>
@stop
