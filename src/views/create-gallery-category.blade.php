@extends(env('ADMIN_LAYOUT'))

@section('content')
	<div class="column">
		<div class="column">
			<h1>Create Gallery Category</h1>
		</div>
		<div class="column">
			<a class="button small mt" href="/admin/gallery/categories">Categories</a>
			<a class="button small mt" href="/admin/galleries">Galleries</a>
		</div>
	</div>
	<create-gallery-category></create-gallery-category>
@stop
