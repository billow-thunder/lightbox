@extends(env('ADMIN_LAYOUT'))

@section('content')
	<edit-gallery :gallery-id="{{ $gallery->id }}"></edit-gallery>
@stop
