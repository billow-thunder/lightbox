@extends(env('ADMIN_LAYOUT'))

@section('content')
	<div class="row expanded">
		<div class="small-6 columns">
			<h1>Create Gallery Category</h1>
		</div>
		<div class="small-6 text-right columns">
			<a class="button small mt" href="/admin/gallery/categories">Categories</a>
			<a class="button small mt" href="/admin/galleries">Galleries</a>
		</div>
	</div>
	<edit-gallery-category :category-id="{{ $category->id }}"></edit-gallery-category>
@stop