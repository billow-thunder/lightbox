@extends(env('ADMIN_LAYOUT'))

@section('content')
	<admin-gallery-images
		:gallery="{{ $gallery->toJson() }}"
		:thumb-size="{{ json_encode(config('lightbox.thumb_size')) }}">
	</admin-gallery-images>
@stop
