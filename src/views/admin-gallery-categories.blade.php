@extends(env('ADMIN_LAYOUT'))

@section('content')
	<div class="columns">
		<div class="column">
			<h1>Gallery Categories</h1>
		</div>
		<div class="column">
			<a class="button small mt" href="/admin/gallery/create-category">Create Category</a>
		</div>
	</div>
	<admin-gallery-categories></admin-gallery-categories>
@stop
