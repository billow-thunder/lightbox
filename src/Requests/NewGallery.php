<?php
namespace Billow\LightBox\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewGallery extends FormRequest
{
	
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required',
		];
	}
}