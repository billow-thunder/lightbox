<?php
namespace Billow\LightBox\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewGalleryCategory extends FormRequest
{
	
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'name' => 'required',
		];
	}
}