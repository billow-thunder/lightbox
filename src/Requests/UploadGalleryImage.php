<?php
namespace Billow\LightBox\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadGalleryImage extends FormRequest
{
	
	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'image' => 'required',
		];
	}
}