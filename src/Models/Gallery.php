<?php
namespace Billow\LightBox\Models;

use Billow\LightBox\Models\GalleryCategory;
use Billow\LightBox\Models\GalleryImage;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
	protected $table = 'billow_galleries';

	protected $fillable = [
		'description',
		'name',
		'path',
		'prefix',
		'slug',
		'cover',
		'sort',
		'published'
	];

	protected $casts = ['published' => 'boolean'];

	public function images()
	{
		return $this->hasMany(GalleryImage::class);
	}

	public function categories()
	{
		return $this->belongsToMany(GalleryCategory::class, 'billow_category_gallery', 'gallery_id', 'category_id');
	}
}