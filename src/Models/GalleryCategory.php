<?php
namespace Billow\LightBox\Models;

use Billow\LightBox\Models\Gallery;
use Illuminate\Database\Eloquent\Model;

class GalleryCategory extends Model
{
	protected $table = 'billow_gallery_categories';

	protected $fillable = [
		'name',
		'slug',
		'cover',
		'description'
	];

	public function galleries()
	{
		return $this->belongsToMany(Gallery::class, 'billow_category_gallery', 'gallery_id', 'category_id');
	}
}