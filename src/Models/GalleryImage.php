<?php
namespace Billow\LightBox\Models;

use Billow\LightBox\Models\Gallery;
use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
	protected $table = 'billow_gallery_images';

	protected $fillable = ['caption', 'sort', 'file', 'gallery_id'];

	protected $casts = ['sort' => 'integer'];

	public function gallery()
	{
		return $this->belongsTo(Gallery::class);
	}
}