<?php

Route::group(['middleware' => ['web', 'auth']], function() {
	Route::get('admin/galleries', 'Billow\LightBox\Controllers\GalleryController@getAdminGalleryIndex');
	Route::post('admin/galleries', 'Billow\LightBox\Controllers\GalleryController@storeNewGallery');
	Route::get('admin/galleries/create', 'Billow\LightBox\Controllers\GalleryController@getCreateGallery');
	Route::get('api/admin/galleries', 'Billow\LightBox\Controllers\GalleryController@loadAdminGalleries');
	Route::get('admin/gallery/categories', 'Billow\LightBox\Controllers\GalleryController@getCategories');
	Route::post('admin/gallery/categories', 'Billow\LightBox\Controllers\GalleryController@postCreateCategory');
	Route::get('admin/gallery/create-category', 'Billow\LightBox\Controllers\GalleryController@createCategory');
	Route::get('admin/galleries/{b_gallery}', 'Billow\LightBox\Controllers\GalleryController@getGalleryEdit');
	Route::get('admin/galleries/{b_gallery}/images', 'Billow\LightBox\Controllers\GalleryController@getAdminGalleryImages');
	Route::get('api/gallery/{b_gallery}/images', 'Billow\LightBox\Controllers\GalleryController@loadGalleryImages');
	Route::post('admin/gallery/{b_gallery}/images', 'Billow\LightBox\Controllers\GalleryController@uploadImage');
	Route::post('admin/gallery/image/{b_gallery_image}/delete', 'Billow\LightBox\Controllers\GalleryController@deleteImage');
	Route::post('admin/gallery/image/{b_gallery_image}/caption', 'Billow\LightBox\Controllers\GalleryController@updateImageCaption');
	Route::get('api/admin/gallery/{b_gallery}', 'Billow\LightBox\Controllers\GalleryController@loadGallery');
	Route::get('api/admin/galleries/categories', 'Billow\LightBox\Controllers\GalleryController@loadCategories');
	Route::post('admin/galleries/{b_gallery}', 'Billow\LightBox\Controllers\GalleryController@postEditGallery');
	Route::post('admin/galleries/{b_gallery}/delete', 'Billow\LightBox\Controllers\GalleryController@deleteGallery');
	Route::get('admin/gallery/category/{b_gallery_category}', 'Billow\LightBox\Controllers\GalleryController@getEditCategory');
	Route::get('api/galleries/categories/{b_gallery_category}', 'Billow\LightBox\Controllers\GalleryController@loadEditCategory');
	Route::post('admin/gallery/categories/{b_gallery_category}', 'Billow\LightBox\Controllers\GalleryController@updateCategory');
	Route::post('admin/gallery/categories/{b_gallery_category}/delete', 'Billow\LightBox\Controllers\GalleryController@deleteCategory');
});