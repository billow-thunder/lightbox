<?php
namespace Billow\LightBox;

use Billow\LightBox\Models\Gallery;
use Billow\LightBox\Models\GalleryCategory;
use Billow\LightBox\Models\GalleryImage;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Billow\Utilities\BillowHelperProvider;
use Billow\Utilities\Facades\StringClean;

class LightBoxProvider extends ServiceProvider
{

	public function register()
	{
		$this->app->register(
			BillowHelperProvider::class
		);
		$this->app->alias('stringclean', StringClean::class);
	}

	public function boot(Router $router)
	{
		$this->loadMigrationsFrom(__DIR__.'/migrations');
		$this->loadRoutesFrom(__DIR__.'/routes.php');
		$this->loadViewsFrom(__DIR__.'/views', 'billow');
		$router->model('b_gallery', Gallery::class);
		$router->model('b_gallery_image', GalleryImage::class);
		$router->model('b_gallery_category', GalleryCategory::class);
		$router->bind('gallery_slug', function ($value) {
		    return Gallery::where('slug', $value)->first();
		});

		$router->bind('gallery_category_slug', function ($value) {
		    return GalleryCategory::where('slug', $value)->first();
		});

		$this->publishes([
				__DIR__.'/config.php' => config_path('lightbox.php'),
				__DIR__.'/img' => public_path('img')
	    ], 'lightbox');
	}
}
