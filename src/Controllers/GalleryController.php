<?php
namespace Billow\LightBox\Controllers;

use App\Http\Controllers\Controller;
use Billow\LightBox\Models\Gallery;
use Billow\LightBox\Models\GalleryCategory;
use Billow\LightBox\Models\GalleryImage;
use Billow\LightBox\Requests\NewGallery;
use Billow\LightBox\Requests\NewGalleryCategory;
use Billow\LightBox\Requests\UpdatedCategory as UpdateCategoryRequest;
use Billow\LightBox\Requests\UpdatedGallery;
use Billow\LightBox\Requests\UploadGalleryImage;
use Billow\Utilities\Facades\StringClean;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class GalleryController extends Controller
{

	public function updateImageCaption(GalleryImage $image, Request $request)
	{
		$image->update(['caption' => $request->caption]);
	}

	public function deleteImage(GalleryImage $image, Request $request, Filesystem $filesystem)
	{
		$filesystem->delete(public_path(config('lightbox.base_path') . $image->gallery->prefix . $image->file));
		$filesystem->delete(public_path(config('lightbox.base_path') . $image->file));
		$image->delete();
	}

	public function getAdminGalleryIndex()
	{
		return view('billow::admin-gallery-index');
	}

	public function loadAdminGalleries()
	{
		return Gallery::orderBy('name', 'asc')->withCount('images')->paginate(15);
	}

	public function getCreateGallery()
	{
		return view('billow::create-gallery');
	}

	public function storeNewGallery(NewGallery $newGallery)
	{
		$slug = StringClean::slug($newGallery->name, new Gallery, 'name');

		return Gallery::create([
			'name' => $newGallery->name,
			'slug' => $slug,
			'path' => config('lightbox.base_path'),
			'description' => $newGallery->description,
			'prefix' => 'thumb_'
		]);
	}

	public function getAdminGalleryImages(Gallery $gallery)
	{
		return view('billow::admin-gallery-images', ['gallery' => $gallery]);
	}

	public function loadGalleryImages(Gallery $gallery)
	{
		return $gallery->images;
	}

	public function uploadImage(Gallery $gallery, UploadGalleryImage $uploadedImage)
	{
		$thumb = Image::make($uploadedImage->image);
		$name = Str::random(10) . '.png';
		$thumb->fit(config('lightbox.thumb_size.width'), config('lightbox.thumb_size.height'))
			->save(public_path(config('lightbox.base_path') . $gallery->prefix . $name));
		$main = Image::make($uploadedImage->image);
		
		if($main->width() < $main->height()) {
			$main->fit(config('lightbox.portrait.width'), config('lightbox.portrait.height'))
				->save(public_path(config('lightbox.base_path') . $name), 100);
		}

		if($main->width() === $main->height()) {
			$main->fit(config('lightbox.square.width'), config('lightbox.square.height'))
				->save(public_path(config('lightbox.base_path') . $name), 100);
		}

		if($main->width() > $main->height()) {
			$main->fit(config('lightbox.landscape.width'), config('lightbox.landscape.height'))
				->save(public_path(config('lightbox.base_path') . $name), 100);
		}

		return $gallery->images()->create([
			'file' => $name
		]);
	}

	public function getGalleryEdit(Gallery $gallery)
	{
		return view('billow::edit-gallery', ['gallery' => $gallery]);
	}

	public function loadGallery(Gallery $gallery)
	{
		return $gallery;
	}

	public function postEditGallery(Gallery $gallery, UpdatedGallery $updatedGallery)
	{
		$gallery->update($updatedGallery->all());
	}

	public function deleteGallery(Gallery $gallery, Filesystem $filesystem)
	{
		$gallery->images->each(function($image) use($gallery, $filesystem) {
			$filesystem->delete(public_path(config('lightbox.base_path') . $gallery->prefix . $image->file));
			$filesystem->delete(public_path(config('lightbox.base_path') . $image->file));
			$image->delete();
		});
		$gallery->delete();
	}

	public function getCategories()
	{
		return view('billow::admin-gallery-categories');
	}

	public function createCategory()
	{
		return view('billow::create-gallery-category');
	}

	public function postCreateCategory(NewGalleryCategory $newGalleryCategory)
	{
		GalleryCategory::create([
			'name' 				=> $newGalleryCategory->name,
			'description' => $newGalleryCategory->description,
			'slug'        => StringClean::slug($newGalleryCategory->name, new GalleryCategory, 'name')
		]);
	}

	public function loadCategories()
	{
		return GalleryCategory::orderBy('name')->withCount('galleries')->paginate(20);
	}

	public function getEditCategory(GalleryCategory $galleryCategory)
	{
		return view('billow::edit-gallery-category', ['category' => $galleryCategory]);
	}

	public function loadEditCategory(GalleryCategory $galleryCategory)
	{
		return [
			'category' => $galleryCategory,
			'active_galleries' => $galleryCategory->galleries->pluck('id')->all(),
			'galleries' => Gallery::get()
		];
	}

	public function updateCategory(GalleryCategory $galleryCategory, UpdateCategoryRequest $updatedCategory) 
	{
		$galleryCategory->update([
			'name' => $updatedCategory->name,
			'description' => $updatedCategory->description,
		]);
		$galleryCategory->galleries()->sync($updatedCategory->galleries);
	}

	public function deleteCategory(GalleryCategory $galleryCategory)
	{
		$galleryCategory->galleries()->detach();
		$galleryCategory->delete();
	}

}