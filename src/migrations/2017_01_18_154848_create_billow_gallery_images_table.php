<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillowGalleryImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billow_gallery_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('caption')->nullable();
            $table->string('file');
            $table->unsignedInteger('gallery_id');
            $table->string('sort')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billow_gallery_images');
    }
}
