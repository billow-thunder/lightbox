<?php

return [
	'thumb_size' => ['width' => 300, 'height' => 300],
	'portrait' => ['width' => 600, 'height' => 800],
	'landscape' => ['width' => 800, 'height' => 600],
	'square' => ['width' => 650, 'height' => 650],
	'base_path' => '/img/lightbox/'
];